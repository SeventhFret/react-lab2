import './Sudoku.css';
// eslint-disable-next-line
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
// eslint-disable-next-line
import Paper from "@mui/material/Paper";
import { Button } from '@mui/material';


// eslint-disable-next-line
const field = [
    [1,  2,  3,  4,  5,  6,  7,  8,  9 ],
    [10, 11, 12, 13, 14, 15, 16, 17, 18],
    [19, 20, 21, 22, 23, 24, 25, 26, 27],
    [28, 29, 30, 31, 32, 33, 34, 35, 36],
    [37, 38, 39, 40, 41, 42, 43, 44, 45],
    [46, 47, 48, 49, 50, 51, 52, 53, 54],
    [55, 56, 57, 58, 59, 60, 61, 62, 63],
    [64, 65, 66, 67, 68, 69, 70, 71, 72],
    [73, 74, 75, 76, 77, 78, 79, 80, 81]
]


export function Sudoku() {


    return (
        // Main box

        <>

        <Box id="Board" >
            { field.map((line, lineInd) => (
                <Box key={lineInd} id="cellsBlock">
                    { line.map((cell) => (
                        <input maxLength={1} type="text" id={cell} placeholder={cell} className='Cell'></input>
                    )) }
                </Box>
            )) }
        </Box>

        {/* <Box>
            { field.map((line, lineInd) => (
                <Box sx={{ height: "4vw", margin: 0}} id={lineInd}>
                    { line.map((square) => (
                        <input maxLength={1} type="text" id={square} className='Cell'></input>
                        // <TextField sx={{  margin: 0, padding: 0, width: "4vw", height: "4vw", borderRadius: '0px' }} key={square}></TextField>
                    )) }
                </Box>
            )) }
        </Box> */}
        <Button sx={{ mt: 5 }} size='large' variant='contained'>Submit results</Button>
        
        </>
    )


}

import { useEffect, useState } from 'react';
import { List } from 'react-virtualized';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import CircularProgress from '@mui/material/CircularProgress';
import Avatar from '@mui/material/Avatar';


const apiUrl = "https://dummyjson.com/users?limit=400&select=firstName,lastName,age,email,image";

function RenderVirtList({ users }) {
    if (!users) { console.log(users); return <CircularProgress /> } 
    else { 
        
        const rowRenderer = ({ index, key, style }) => {
            const user = users["users"][index]

            return (
                <div key={key} style={style}>
                    <div style={{ borderBottom: '1px solid white', padding: '1vw' }}>
                        <Box display='flex' alignItems='center' direction='row'>
                            <Avatar sx={{ border: '1px solid blue', mr: '1vw' }} src={user["image"]} alt='User avatar' />
                            <Stack>
                                <Typography variant='subtitle1'>{user["firstName"]} {user["lastName"]}</Typography>
                                <Typography variant='body2'>{user["email"]}</Typography>
                                <Typography variant='body2'>{user["age"]} years old</Typography>
                            </Stack>
                        </Box>
                    </div>
                </div>
            );
        }

        return (
            <List height={500}
            width={800}
            rowHeight={100}
            rowCount={users['total']}
            rowRenderer={rowRenderer}
            style={{ color: 'white', border: '2px solid white', borderRadius: '10px' }}
            />
    )}
}


export function VirtList() {
    const [users, setUsers] = useState();
    
    const getData = () => {
        fetch(apiUrl)
        .then(res => res.json())
        .then(usersData => setUsers(usersData));
    }

    useEffect(() => {
        getData();
    }, []);
    
    
    return (
        <div>
            <div>
                <h3 style={{ color: 'white' }}>Rows: { users ? users['total'] : null}</h3>
            </div>
            { users ? <RenderVirtList users={users}/> : <CircularProgress /> }
        </div>
    )
}

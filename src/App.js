import './App.css';
// import { VirtList } from './components/VirtList';
import { Sudoku } from './components/Sudoku';



function App() {
  return (
    <>
      {/* <div className="Flex Flex-col Main-section">
        <h1 className='Main-header'>Welcome to my App!</h1>
        <h2 style={{ color: 'white' }}>Virtual list</h2>
        <VirtList />
      </div> */}

      <div className="Flex Flex-col Main-section">
        <h2 style={{ color: 'white' }}>Sudoku</h2>
        <Sudoku />
      </div>
    </>
  );
}

export default App;
